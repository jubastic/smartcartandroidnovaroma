package juba.br.com.smartcart.model;

import android.graphics.drawable.Drawable;

/**
 * Created by Juba on 02/12/2015.
 */

    public class Product {

        public String title;
        public double price;
        public String description;
        public Drawable prodImage;
        public boolean selected;

        public Product(String title, String description, Drawable prodImage, double price) {
            this.title = title;
            this.description = description;
            this.price = price;
            this.prodImage=prodImage;
        }

    public Product(String title, String description, double price) {
        this.title = title;
        this.description = description;
        this.price = price;

    }

    /**
     * Created by Juba on 10/12/2015.
     */
    public static class CartEntry {

        private Product product;
        private int quantity;

        public CartEntry(Product product, int quantity) {
            this.product = product;
            setQuantity(quantity);
        }

        public Product getProduct() {
            return product;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
    }
}

