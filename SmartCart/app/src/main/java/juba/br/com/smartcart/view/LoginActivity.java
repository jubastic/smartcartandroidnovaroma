package juba.br.com.smartcart.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import juba.br.com.smartcart.R;
import juba.br.com.smartcart.controller.DatabaseHandler;

public class LoginActivity extends AppCompatActivity {

    DatabaseHandler db = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    public void onButtonClick(View v){

        if (v.getId() == R.id.btnConfirmar){

            EditText username = (EditText) findViewById(R.id.editTextUser);
            String n = username.getText().toString();
            EditText pass = (EditText) findViewById(R.id.editTextPass);
            String p = pass.getText().toString();

           String password = db.searchPassword(n);
            String name = db.searchName(n);

            if(p.equals(password)){
                Intent i = new Intent(LoginActivity.this, HistoryActivity.class);
                i.putExtra("username", name);
                startActivity(i);
            }else{

                Toast.makeText(LoginActivity.this, "Login e/ou password Inválidos!", Toast.LENGTH_SHORT).show();
            }

        }

        if(v.getId() == R.id.btnCadastrar){

            Intent i = new Intent(LoginActivity.this, SignUp.class);
            startActivity(i);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
