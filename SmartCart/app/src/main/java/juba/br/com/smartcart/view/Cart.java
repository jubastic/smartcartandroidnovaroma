package juba.br.com.smartcart.view;

import android.os.Bundle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import juba.br.com.smartcart.R;
import juba.br.com.smartcart.controller.ProdAdapter;
import juba.br.com.smartcart.controller.SmartCartHelperClass;
import juba.br.com.smartcart.model.Product;

public class Cart extends Activity {

    private List<Product> mCartList;
    private ProdAdapter mProductAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);


        mCartList = SmartCartHelperClass.getCartList();

        //limpa os itens selecionados
        for(int i=0; i<mCartList.size(); i++) {
           mCartList.get(i).selected = false;
        }

        //Cria a lista

        final ListView listViewCatalog = (ListView) findViewById(R.id.ListViewCatalog);
        mProductAdapter = new ProdAdapter(mCartList, getLayoutInflater(), true);
        listViewCatalog.setAdapter(mProductAdapter);

        listViewCatalog.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent productDetailsIntent = new Intent(getBaseContext(), ProdDetails.class);
                productDetailsIntent.putExtra(SmartCartHelperClass.INDEX_DO_PRODUCT, position);
                startActivity(productDetailsIntent);
            }
        });

        if(mProductAdapter != null) {
            mProductAdapter.notifyDataSetChanged();
        }

        double subTotal = 0;
        for(Product p : mCartList) {
            int quantity = SmartCartHelperClass.getProductQuantity(p);
            subTotal += p.price * quantity;
        }

        final String subt = String.valueOf(subTotal);

        Button buttonCheckout = (Button) findViewById(R.id.ButtonCheckout);

        buttonCheckout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
                Date date = new Date();
                String data = dateFormat.format(date);

                Intent HistoryIntent = new Intent(Cart.this, HistoryActivity.class);
                HistoryIntent.putExtra("subtotal", subt);
                HistoryIntent.putExtra("data", data);
                Toast.makeText(Cart.this, "Obrigado por efetuar a compra!", Toast.LENGTH_SHORT).show();
                startActivity(HistoryIntent);

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();


        if(mProductAdapter != null) {
            mProductAdapter.notifyDataSetChanged();
        }

        double subTotal = 0;
        for(Product p : mCartList) {
            int quantity = SmartCartHelperClass.getProductQuantity(p);
            subTotal += p.price * quantity;
        }

        TextView productPriceTextView = (TextView) findViewById(R.id.TextViewSubtotal);
        productPriceTextView.setText("Subtotal: R$" + subTotal);



    }

}
