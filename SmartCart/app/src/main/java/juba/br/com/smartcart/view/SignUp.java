package juba.br.com.smartcart.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import juba.br.com.smartcart.R;
import juba.br.com.smartcart.controller.DatabaseHandler;
import juba.br.com.smartcart.model.User;

/**
 * Created by Juba on 04/12/2015.
 */


public class SignUp extends Activity {

    DatabaseHandler db = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

      //  SQLiteOpenHelper db = new DatabaseHelper(getApplicationContext());
     //   db.onUpgrade(db.getWritableDatabase(), 1, 1);
    }

    public void onSignUpClick(View v){

        if(v.getId() == R.id.btnCadastrarUser){


            EditText name = (EditText) findViewById(R.id.editTextName);
            EditText username = (EditText) findViewById(R.id.editTextUsername2);
            EditText password = (EditText) findViewById(R.id.editTextPassword);
            EditText password2 = (EditText) findViewById(R.id.editTextPasswordConfirm);

            String n = name.getText().toString();
            String u = username.getText().toString();
            String p1 = password.getText().toString();
            String p2 = password2.getText().toString();

            String testLogin = db.searchLogin(u);

            if(!p1.equals(p2)){
                Toast.makeText(SignUp.this, "As senhas são incompatíveis!", Toast.LENGTH_SHORT).show();
            }


            if(u.equals(testLogin)){

                Toast.makeText(SignUp.this, "Já existe usuário "+testLogin+" cadastrado, escolha outro Username!", Toast.LENGTH_SHORT).show();
            }
            else{

                User user = new User();
                user.setName(n);
                user.setPassword(p1);
                user.setUsername(u);


                db.insertUser(user);
                Toast.makeText(SignUp.this, "Usuário cadastrado com sucesso!", Toast.LENGTH_SHORT).show();

                finish();
            }
        }
    }
}
