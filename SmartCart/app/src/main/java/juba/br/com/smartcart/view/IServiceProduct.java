package juba.br.com.smartcart.view;

import juba.br.com.smartcart.model.Product;

/**
 * Created by Juba on 10/12/2015.
 */
public interface IServiceProduct {

    public Product getProduct(Product product);
    public Product getProductByName(String s);


}
