package juba.br.com.smartcart.controller;

import android.content.res.Resources;
import android.support.v4.content.res.ResourcesCompat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import juba.br.com.smartcart.R;
import juba.br.com.smartcart.model.Product;

/**
 * Created by Juba on 02/12/2015.
 */

public class SmartCartHelperClass {

    public static final String INDEX_DO_PRODUCT = "PRODUCT_INDEX";

    private static List<Product> catalog;
   // private static List<Product> cart;
    private static Map<Product, Product.CartEntry> cartMap = new HashMap<Product, Product.CartEntry>();

    public static List<Product> getCatalogo(Resources res) {


        if (catalog == null) {
            catalog = new Vector<Product>();
            catalog.add(new Product("Vikings - Season 1 ",
                    "DvD & Blu-ray completos da segunda temporada da serie Vikings do Canal History Channel, contendo 10 episódios, um total de 420:00 minutos de pura adrenalina.",
                    ResourcesCompat.getDrawable(res, R.drawable.vikings,null), 199.99));
            catalog.add(new Product("Vikings - Season 2",
                    "DvD & Blu-ray  completos da segunda temporada da serie Vikings do Canal History Channel, contendo 10 episódios, um total de 420:00 minutos de pura adrenalina.",
                    ResourcesCompat.getDrawable(res,R.drawable.vikings2,null), 229.99));
          //  catalog.add(new Product("Game of Thrones - Season 1",
           //         "DvD & Blu-ray  completos da primeira temporada da serie Game of Thrones,  contendo 10 episódios, um total de 590:00 minutos de crônica!",
            //        res.getDrawable(R.drawable.got1),213.99));
            //catalog.add(new Product("Game of Thrones - Season 2",
           //         "DvD & Blu-ray  completos da segunda temporada da serie Game of Thrones,  contendo 10 episódios, um total de 590:00 minutos de crônica!",
             //       res.getDrawable(R.drawable.got2), 149.99));
           // catalog.add(new Product("Game of Thrones - Seanson 3",
           //         "DvD & Blu-ray  completos da terceira temporada da serie Game of Thrones,  contendo 10 episódios, um total de 590:00 minutos de crônica!",
             //       res.getDrawable(R.drawable.got3),159.99));
           // catalog.add(new Product("Game of Thrones - Seanson 4",
               //     "DvD & Blu-ray  completos da quarta temporada da serie Game of Thrones,  contendo 10 episódios, um total de 590:00 minutos de crônica!",
                 //   res.getDrawable(R.drawable.got4),199.99));
           // catalog.add(new Product("Game of Thrones - Seanson 5",
                   // "DvD & Blu-ray  completos da quinta temporada da serie Game of Thrones,  contendo 10 episódios, um total de 590:00 minutos de crônica!",
          //          res.getDrawable(R.drawable.got5),229.99));
        }

        return catalog;
    }

    public static List<Product> getCatalogo2(String title, String desc, Double price){


        if(catalog==null) {
            catalog = new Vector<Product>();
            catalog.add(new Product(title, desc, price));
        }

        return catalog;
    }

    public static void setQuantity(Product product, int quantity) {
        // pega a atual entrada do carrinho
        Product.CartEntry curEntry = cartMap.get(product);

        // se a quantd for 0 ou >0 remove tudo
        if (quantity <= 0) {
            if (curEntry != null)
                removeProduct(product);
            return;
        }

        // se não existir entrada, cria uma
        if (curEntry == null) {
            curEntry = new Product.CartEntry(product, quantity);
            cartMap.put(product, curEntry);
            return;
        }


        curEntry.setQuantity(quantity);
    }

    public static int getProductQuantity(Product product) {

        Product.CartEntry curEntry = cartMap.get(product);

        if (curEntry != null)
            return curEntry.getQuantity();

        return 0;
    }

    public static void removeProduct(Product product) {
        cartMap.remove(product);
    }

    public static List<Product> getCartList() {
        List<Product> cartList = new Vector<Product>(cartMap.keySet().size());

        for (Product p : cartMap.keySet()) {
            cartList.add(p);
        }
    return cartList;
    }
}
