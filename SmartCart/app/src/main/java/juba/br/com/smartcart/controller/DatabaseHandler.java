package juba.br.com.smartcart.controller;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import juba.br.com.smartcart.model.User;

import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

/**
 * Created by Juba on 04/12/2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION= 3;
    private static final String DATABASE_NAME = "users-db";
    private static final String TABLE_USER = "usertable";
    SQLiteDatabase db;
    private static final String KEY_ID = "_id";
    private static final String KEY_NAME = "name";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "pass";



        public DatabaseHandler(Context context){

            super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    public void onCreate(SQLiteDatabase db){

        String TABLE_CREATE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_USER + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME +
                        " TEXT,"+ KEY_USERNAME +" TEXT," + KEY_PASSWORD+ " TEXT" + ")";

         db.execSQL(TABLE_CREATE);

    }

    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_USER);
        onCreate(db);
        this.db = db;
    }

    public void insertUser(User user){

        db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        String query = "select * from "+TABLE_USER;
        Cursor cursor = db.rawQuery(query, null);
        int aux = cursor.getCount();

        cv.put(KEY_ID, aux);
        cv.put(KEY_NAME, user.getName());
        cv.put(KEY_USERNAME, user.getUsername());
        cv.put(KEY_PASSWORD, user.getPassword());


        db.insert(TABLE_USER, null, cv);
        db.close();
        cursor.close();

    }

    public String searchLogin(String s){

        db = this.getReadableDatabase();
        String query = "select username from "+TABLE_USER+";";
        String x;
        String y ="";
        Cursor cursor = db.rawQuery(query, null);


        if(cursor.moveToFirst()){

            do{
                x = cursor.getString(0);

              if (x.equals(s)){
                y = cursor.getString(0);
                    break;
                }
            }while(cursor.moveToNext());
        }
        cursor.close();
    return y;

    }


    public String searchPassword(String s){

        db = this.getReadableDatabase();
        String query = "select username , pass from "+TABLE_USER+";";

        //String query = "select username , pass from "+TABLE_USER+" where pass = ?;";
        Cursor cursor = db.rawQuery(query, null);
        // Cursor cursor = db.rawQuery(query, new String[] {s});
        String x;
        String pass = "Não foi encontrado!";


        if(cursor.moveToFirst()){

            do{
                x = cursor.getString(0);

                if (x.equals(s)){
                    pass = cursor.getString(1);
                    break;
                }
            }while(cursor.moveToNext());
        }
                cursor.close();
            return pass;

    }

    public String searchName(String s){

        db = this.getReadableDatabase();
        String query = "select name , username from "+TABLE_USER+";";

        Cursor cursor = db.rawQuery(query, null);

        String x;
        String y = "Error!";


        if(cursor.moveToFirst()){

            do{
                x = cursor.getString(1);

                if (x.equals(s)){
                   y  = cursor.getString(0);
                    break;
                }
            }while(cursor.moveToNext());
        }
        cursor.close();
        return y;


    }

}
