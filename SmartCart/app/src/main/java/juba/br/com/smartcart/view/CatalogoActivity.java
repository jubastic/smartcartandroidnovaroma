package juba.br.com.smartcart.view;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import juba.br.com.smartcart.R;
import juba.br.com.smartcart.controller.ProdAdapter;
import juba.br.com.smartcart.controller.SmartCartHelperClass;
import juba.br.com.smartcart.model.Product;

public class CatalogoActivity extends AppCompatActivity {

    private List<Product> mProductList;

    static final Uri CONTENT_URL = Uri.parse(
     "content://juba.br.com.smartcartprovider.ProductsProvider/cproducts"
    );
    //CursorLoader cursorLoader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogo);

        mProductList = SmartCartHelperClass.getCatalogo(getResources());

        ListView listViewCatalog = (ListView) findViewById(R.id.ListViewCatalogo);
        listViewCatalog.setAdapter(new ProdAdapter(mProductList, getLayoutInflater(), false));

        final String username = getIntent().getStringExtra("username");
        TextView text = (TextView) findViewById(R.id.TextViewNameCat);
        text.setText(username);

        listViewCatalog.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent prodDetailsIntent = new Intent(getBaseContext(), ProdDetails.class);
                prodDetailsIntent.putExtra(SmartCartHelperClass.INDEX_DO_PRODUCT, position);
                startActivity(prodDetailsIntent);
            }
        });

        Button viewShoppingCart = (Button) findViewById(R.id.ButtonViewCart);
        viewShoppingCart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent viewShoppingCartIntent = new Intent(getBaseContext(), Cart.class);
                startActivity(viewShoppingCartIntent);
            }
        });


        ImageButton phonebutton = (ImageButton) findViewById(R.id.imageButtonPhone);
        phonebutton.setOnClickListener( new View.OnClickListener(){

            public void onClick(View v) {
                String[] projection = new String[]{"title", "desc", "price"};

                try {
                    ContentResolver resolver = getContentResolver();
                    Cursor cursor = resolver.query(CONTENT_URL, projection, null, null, null);
                    if(cursor == null){
                        Toast.makeText(CatalogoActivity.this, "Não foi possível realizar a ação, tente novamente mais tarde!", Toast.LENGTH_LONG).show();
                    }


                    if (cursor.moveToFirst()) {

                        do {

                            String titleList = cursor.getString(cursor.getColumnIndex("title"));
                            String descList = cursor.getString(cursor.getColumnIndex("desc"));
                            String priceList = cursor.getString(cursor.getColumnIndex("price"));
                            double valuePrice = Double.parseDouble(priceList);

                            mProductList = SmartCartHelperClass.getCatalogo2(titleList, descList, valuePrice);


                        } while (cursor.moveToNext());

                    }

                    ListView listViewCatalog = (ListView) findViewById(R.id.ListViewCatalogo);
                    listViewCatalog.setAdapter(new ProdAdapter(mProductList, getLayoutInflater(), false));


                }catch(Exception e) {
                    Toast.makeText(CatalogoActivity.this, "Não foi possível realizar a ação, tente novamente mais tarde!", Toast.LENGTH_LONG).show();
                }
            }


        });


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_catalogo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
