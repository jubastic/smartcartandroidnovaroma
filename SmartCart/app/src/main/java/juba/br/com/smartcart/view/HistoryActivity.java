package juba.br.com.smartcart.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import juba.br.com.smartcart.R;

public class HistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);

        final String username = getIntent().getStringExtra("username");

        TextView text = (TextView) findViewById(R.id.TextViewUserName);
        text.setText(username);

        Button buttonForward = (Button) findViewById(R.id.btnSeguir);

        buttonForward.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(HistoryActivity.this, CatalogoActivity.class);
                i.putExtra("username", username);
                startActivity(i);
            }
        });

        String data = getIntent().getStringExtra("data");
        String subTotal = getIntent().getStringExtra("subtotal");



        String[] list = new String[]{"Data da compra: " + data + " Total da compra: " + subTotal};

        ListView listViewCatalog = (ListView) findViewById(R.id.ListViewCatalogHistory);
        listViewCatalog.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list));
        listViewCatalog.refreshDrawableState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onStart(){
        super.onStart();

        String data = getIntent().getStringExtra("data");
        String subTotal = getIntent().getStringExtra("subtotal");

        String[] list = new String []{"Data da compra: "+data+ " Total da compra: "+subTotal};


        ListView listViewCatalog = (ListView) findViewById(R.id.ListViewCatalogHistory);
        listViewCatalog.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list));

    }

    public void onResume(){
        super.onResume();



    }


}
